< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

#- load the instance definition
< instance.cmd

epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME)")
epicsEnvSet("PORT",                         "$(PREFIX)-asyn-port")
epicsEnvSet("STREAM_PROTOCOL_PATH",         "$(DB_DIR)")

# ?? epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

#- Create a asyn driver
drvAsynIPPortConfigure("$(PORT)","$(DEVICE_IP):$(DEVICE_IPPORT)", 0, 0, 0)

# Load needed records
dbLoadRecords("keysight-33522B.db", "DEVICE=$(PREFIX), PORT=$(PORT)")

# report streamdevice errors
var streamError 1
# report streamdevice debug messages (LOTS!)
# var streamDebug 1

###############################################################################
iocInit
###############################################################################
date
###############################################################################
